<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultRole = new Role();
        $defaultRole->ident = 'user';
        $defaultRole->caption = 'Default user Role';
        $defaultRole->save();

        $adminRole = new Role();
        $adminRole->ident = 'admin';
        $adminRole->caption = 'Admin role';
        $adminRole->save();
    }
}
