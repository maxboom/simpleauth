<?php

use Illuminate\Database\Seeder;

use App\UserActionType;

class UserActionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserActionType::insert([
            [
                'ident' => 'successful_login',
                'caption' => 'Successful Login'
            ],
            [
                'ident' => 'login_failed',
                'caption' => 'Loging Failed'
            ],
            [
                'ident' => 'pageview',
                'caption' => 'Pageview'
            ]
        ]);
    }
}
