<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all()->keyBy('ident'); 

        $userRole = $roles->get('user');
        $adminRole = $roles->get('admin');

        //for sample
        $firstUser = new User();
        $firstUser->name = 'First User';
        $firstUser->email = 'firstuser@mailtrap.io';
        $firstUser->password = bcrypt('123456');
        $firstUser->save();
        $firstUser->roles()->attach($userRole);

        $secondUser = new User();
        $secondUser->name = 'Second User';
        $secondUser->email = 'seconduser@mailtrap.io';
        $secondUser->password = bcrypt('qwerty');
        $secondUser->save();
        $secondUser->roles()->attach($adminRole);
    }
}
