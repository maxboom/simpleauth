<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $userAction)
    {
        return $user->roles()->whereIdent('admin')->first();
    }
}
