<?php

namespace App\Policies;

use App\User;
use App\UserAction;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserActionPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $userAction)
    {
        //
    }

    /**
     * Determine whether the user can view the userAction.
     *
     * @param  \App\User  $user
     * @param  \App\UserAction  $userAction
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->roles()->whereIdent('admin')->first();
    }

    /**
     * Determine whether the user can create userActions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the userAction.
     *
     * @param  \App\User  $user
     * @param  \App\UserAction  $userAction
     * @return mixed
     */
    public function update(User $user, UserAction $userAction)
    {
        //
    }

    /**
     * Determine whether the user can delete the userAction.
     *
     * @param  \App\User  $user
     * @param  \App\UserAction  $userAction
     * @return mixed
     */
    public function delete(User $user, UserAction $userAction)
    {
        //
    }
}
