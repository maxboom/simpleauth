<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\PageView;
use GuzzleHttp\Client as HttpClient;
use Laravel\Passport\Client;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        event(new PageView($user));
        return view('home');
    }

    public function oauthCallback(Request $request)
    {
        $client = Client::findOrFail(config('app.oauth_client_id'));
        
        $httpClient = new HttpClient();
        $response = $httpClient->post(url('oauth/token'), [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'redirect_uri' => $client->redirect,
                'code' => $request->code
            ]
        ]);

        $token = json_decode($response->getBody());

        $apiReponse = $httpClient->request('GET', url('/api/user'), [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => "Bearer $token->access_token"
            ]
        ]);

        return json_decode($apiReponse->getBody(), true);
    }
}
