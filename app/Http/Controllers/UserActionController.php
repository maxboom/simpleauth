<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserActionType;
use App\UserAction;
use App\User;
use Input;

class UserActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->cannot('view', UserAction::class)) {
            return view('home');
        }

        $inputs = $request->all();
        $this->_prepareFilterInputs($inputs);

        $query = $this->_getFilterQuery($inputs);

        $data = $query->paginate(config('app.paginate'));

        $data->appends($request->input());

        $users = User::all();

        return view('dashboard.gridview')->with([
                'data' => $data,
                'title' => 'User Actions',
                'alias' => 'useractions',
                'model' => UserAction::class,
                'columns' => [
                    'id' => 'ID',
                    'user_id' => 'User ID',
                    'action_type_ident' => 'Action',
                    'email' => 'Email',
                    'password' => 'Password',
                    'created_at' => 'Date of Request'
                ],
                'filters' => [
                    'action_type_ident' => [
                        'ident' => 'ident',
                        'caption' => 'caption',
                        'type' => 'select',
                        'data' => UserActionType::all()
                    ],
                    'created_at' => [
                        'type' => 'date',
                    ],
                    'user_id' => [
                        'type' => 'select',
                        'ident' => 'id',
                        'caption' => 'email',
                        'data' => User::all()
                    ]
                ]
            ]
        );
    }

    private function _getFilterQuery($inputs)
    {
        $query = new UserAction();

        if (array_key_exists('created_at', $inputs)) {
            $query = $query->whereDate(
                'created_at', '=', $inputs['created_at']
            );

            unset($inputs['created_at']);
        }

        return $query->where($inputs)->orderBy('created_at', 'desc');
    } // end _getFilterQuery

    private function _prepareFilterInputs(&$inputs)
    {
        $inputs = array_filter($inputs, function($input) {
            return $input;
        });
        
        unset($inputs['page']);

        return true;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
