<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->cannot('view', User::class)) {
            return view('home');
        }

        $inputs = $request->all();
        $this->_prepareFilterInputs($inputs);
        $query = $this->_getFilterQuery($inputs);

        $data = $query->paginate(config('app.paginate'));

        $data->appends($request->input());

        return view('dashboard.gridview')->with([
                'data' => $data,
                'title' => 'Users',
                'alias' => 'users',
                'model' => User::class,
                'columns' => [
                    'id' => 'ID',
                    'name' => 'Name',
                    'email' => 'Email'
                ],
                'filters' => [
                    'email' => [
                        'type' => 'select',
                        'ident' => 'email',
                        'caption' => 'email',
                        'data' => User::all()
                    ]
                ]
            ]
        );
    }

    private function _getFilterQuery($inputs)
    {
        $query = new User();

        if (array_key_exists('created_at', $inputs)) {
            $query = $query->whereDate(
                'created_at', '=', $inputs['created_at']
            );

            unset($inputs['created_at']);
        }

        return $query->where($inputs);
    } // end _getFilterQuery

    private function _prepareFilterInputs(&$inputs)
    {
        $inputs = array_filter($inputs, function($input) {
            return $input;
        });
        
        unset($inputs['page']);

        return true;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $errors = array())
    {
        if ($request->user()->cannot('create', User::class)) {
            return view('home');
        }

        return view('dashboard.create')->with([
            'data' => new User(),
            'model' => User::class,
            'title' => 'User Edit',
            'alias' => 'users',
            'columns' => [
                'name' => 'Name',
                'email' => 'Email',
                'password' => 'Password',
                'password_confirmation' => 'Password Confirmation',
                'role' => 'User Role'
            ],
            'edit' => [
                'name' => [
                    'type' => 'text',
                    
                ],
                'email' => [
                    'type' => 'email',
                ],
                'password' => [
                    'type' => 'password'
                ],
                'password_confirmation' => [
                    'type' => 'password'
                ],
                'role' => [
                    'type' => 'select',
                    'ident' => 'ident',
                    'caption' => 'caption',
                    'data' => Role::all()
                ]
            ],
            'errors' => $errors
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->_prepareStoreInputs($request);

        if ($errors) {
            return $this->create($request, $errors);
        }

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password'])
        ]);

        $defaultRole = Role::whereIdent($request['role'])->first();
        $user->roles()->attach($defaultRole);

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $errors = array())
    {
        $user = User::findOrFail($id);

        return view('dashboard.edit')->with([
            'data' => $user,
            'model' => User::class,
            'title' => 'User Edit',
            'alias' => 'users',
            'columns' => [
                'name' => 'Name',
                'email' => 'Email',
                'password' => 'Password',
                'password_confirmation' => 'Password Confirmation'
            ],
            'edit' => [
                'name' => [
                    'type' => 'text',
                    
                ],
                'email' => [
                    'type' => 'email',
                ],
                'password' => [
                    'type' => 'password'
                ],
                'password_confirmation' => [
                    'type' => 'password'
                ]
            ],
            'errors' => $errors
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->cannot('update', User::class)) {
            return view('home');
        }

        $errors = $this->_prepareUpdateInputs($request);

        if ($errors) {
            return $this->edit($id, $errors);
        }

        $user = User::find($id);
        $user->name = $request['name'];
        $user->email = $request['email'];

        if ($request->get('password')) {
            $user->password = bcrypt($request['password']);
        }

        $user->save();

        return redirect(route('users.index'));
    }

    private function _prepareStoreInputs($request, $validator = null)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|confirmed',
                'role' => 'required|exists:roles,ident'
            ]
        );

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }

    private function _prepareUpdateInputs($request, $validator = null)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,id' . $request->get('id'),
                'password' => 'confirmed'
            ]
        );

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->cannot('delete', User::class)) {
            return view('home');
        }

        $user = User::findOrFail($id);
        $user->delete();

        return redirect(route('users.index'));
    }
}
