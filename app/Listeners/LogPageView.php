<?php

namespace App\Listeners;

use App\Events\PageView;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\UserAction;
use Auth;

class LogPageView
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PageView  $event
     * @return void
     */
    public function handle(PageView $event)
    {
        UserAction::create([
            'user_id' => Auth::id(),
            'action_type_ident' => 'pageview'
        ]);
    }
}
