<?php
    $input['data']->push([
        $input['ident'] => '',
        $input['caption'] => '------------'
    ]);
?>

{!!

    Form::select(
        $inputName, 
        $input['data']->pluck($input['caption'], $input['ident']),
        null
    )
!!}