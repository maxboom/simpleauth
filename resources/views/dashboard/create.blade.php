@extends('layouts.dashboard')

@section('panel-heading', $title)

@section('panel-body')
    {!! Form::model($data, ['route' => ["$alias.store", $data], 'method' => 'POST']) !!}

        @if (isset($errors) && $errors)
        <div class="alert alert-danger">
                <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif

        @foreach ($edit as $inputName => $input)

            <div class="form-group">
                <div class="col-lg-2">
                {!! Form::label($inputName, $columns[$inputName]) !!}
                </div>
                @include('dashboard.hui.' . $input['type'])
            </div>

        @endforeach

        {!! Form::submit('Save') !!}

    {!! Form::close() !!}
@endsection