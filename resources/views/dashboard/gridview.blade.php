@extends('layouts.dashboard')

@section('panel-heading', $title)

@section('panel-body')
    <div class="panel panel-heading">

        <div class="row">
            <div class="col-lg-10">
            {!! Form::open(['url' => route("$alias.index"), 'method' => 'get']) !!}

                @foreach ($filters as $inputName => $input)
                
                    {!! Form::label($columns[$inputName]) !!}

                    @include('dashboard.hui.' . $input['type'])

                @endforeach

                {!! Form::submit('Filter') !!}

            {!! Form::close() !!}
            </div>
            <div class="col-lg-2">
                @can('create', new $model())
                    {!! Form::open(['url' => route("$alias.create"), 'method' => 'get']) !!}
                            <button class="btn btn-success  pull-right" type="submit">Create</button>
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>

    </div>

    <table class="table">
        <thead>
            @foreach ($columns as $column)
                <th>{{ $column }}</th>
            @endforeach

            @can('update', new $model())
                <th>#Update</th>
            @endcan

            @can('delete', new $model())
                <th>#Delete</th>
            @endcan
        </thead>
        
        <tbody>

            @foreach ($data as $row)
            <tr>
                @foreach ($columns as $column => $caption)
                    <td>{{ $row[$column] }}</td>
                @endforeach

                @can('update', $row)
                    <th>
                        {!! Form::open(['url' => route("$alias.edit", $row->id), 'method' => 'get']) !!}
                            <button class="btn " type="submit">Edit</button>
                        {!! Form::close() !!}
                    </th>
                @endcan

                @can('delete', $row)
                    <th>
                        {!! Form::open(['url' => route("$alias.destroy", $row->id), 'method' => 'delete']) !!}
                            <button class="btn btn-danger" type="submit">Delete</button>
                        {!! Form::close() !!}
                    </th>
                @endcan
            </tr>
            @endforeach

        </tbody>

    </table>

    {{ $data->links()  }} 
@endsection