<div class="col-lg-2">
    <ul class="nav navbar-default">
        @can('view', new \App\UserAction())
        <li><a href="{{ route('useractions.index') }}">User Actions</a></li>
        @endcan
        @can('view', new \App\User())
        <li><a href="{{ route('users.index') }}">Users</a></li>
        @endcan
    </ul>
</div>