@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        
        <div class="row">
            @include('layouts.navigation')
            <div class="col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">@yield('panel-heading')</h3>
                    </div>
                    <div class="panel-body">
                        @yield('panel-body')
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

