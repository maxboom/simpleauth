<?php

use Laravel\Passport\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    $oauthClient = Client::find(config('app.oauth_client_id'));

    $query = http_build_query([
        'client_id' => $oauthClient->id,
        'redirect_uri' => $oauthClient->redirect,
        'response_type' => 'code',
    ]);

    return view('welcome')->with([
        'oauthQuery' => url('oauth/authorize') . "?$query"
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::any('/auth/callback', 'HomeController@oauthCallback');

Route::resource('useractions', 'UserActionController');

Route::resource('users', 'UserController');